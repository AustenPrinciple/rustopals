//! This file contains my implementation for AES.
//! I am using the particular algorithm using the optimisation described
//! here -> https://en.wikipedia.org/wiki/Advanced_Encryption_Standard#cite_ref-15

mod tables;
use crate::aes::tables::*;
use crate::utils;

/// Apply the AES S-box algorithm to each byte in the state.
pub fn sub_bytes(state: &mut [u8]) {
    for e in state.iter_mut() {
        *e = S_BOX[*e as usize];
    }
}

/// Combine the Shift Rows algorithm with the SubBytes step.
pub fn box_shift_rows(state: &mut [u8]) {
    // just substitute row 0
    state[0] = S_BOX[state[0] as usize];
    state[4] = S_BOX[state[4] as usize];
    state[8] = S_BOX[state[8] as usize];
    state[12] = S_BOX[state[12] as usize];

    // rotate row 1
    let tmp = S_BOX[state[1] as usize];
    state[1] = S_BOX[state[5] as usize];
    state[5] = S_BOX[state[9] as usize];
    state[9] = S_BOX[state[13] as usize];
    state[13] = tmp;

    // rotate row 2
    let tmp = S_BOX[state[2] as usize];
    state[2] = S_BOX[state[10] as usize];
    state[10] = tmp;
    let tmp = S_BOX[state[6] as usize];
    state[6] = S_BOX[state[14] as usize];
    state[14] = tmp;

    // rotate row 3
    let tmp = S_BOX[state[15] as usize];
    state[15] = S_BOX[state[11] as usize];
    state[11] = S_BOX[state[7] as usize];
    state[7] = S_BOX[state[3] as usize];
    state[3] = tmp;
}

/// Combine the inverse Shift Rows algorithm with the SubBytes step.
pub fn inverse_box_shift_rows(state: &mut [u8]) {
    // restore row 0
    state[0] = INVERSE_S_BOX[state[0] as usize];
    state[4] = INVERSE_S_BOX[state[4] as usize];
    state[8] = INVERSE_S_BOX[state[8] as usize];
    state[12] = INVERSE_S_BOX[state[12] as usize];

    // restore row 1
    let tmp = INVERSE_S_BOX[state[13] as usize];
    state[13] = INVERSE_S_BOX[state[9] as usize];
    state[9] = INVERSE_S_BOX[state[5] as usize];
    state[5] = INVERSE_S_BOX[state[1] as usize];
    state[1] = tmp;

    // restore row 2
    let tmp = INVERSE_S_BOX[state[2] as usize];
    state[2] = INVERSE_S_BOX[state[10] as usize];
    state[10] = tmp;
    let tmp = INVERSE_S_BOX[state[6] as usize];
    state[6] = INVERSE_S_BOX[state[14] as usize];
    state[14] = tmp;

    // restore row 3
    let tmp = INVERSE_S_BOX[state[3] as usize];
    state[3] = INVERSE_S_BOX[state[7] as usize];
    state[7] = INVERSE_S_BOX[state[11] as usize];
    state[11] = INVERSE_S_BOX[state[15] as usize];
    state[15] = tmp;
}

/// Combine all three normal steps of AES:
/// SubBytes
/// ShiftRows
/// MixColumns
pub fn round_operation(state: &mut [u8]) {
    let tmp = vec![
        // mixing column 0
        (X_TIME_2_S_BOX[state[0] as usize]
            ^ X_TIME_3_S_BOX[state[5] as usize]
            ^ S_BOX[state[10] as usize]
            ^ S_BOX[state[15] as usize]),
        (S_BOX[state[0] as usize]
            ^ X_TIME_2_S_BOX[state[5] as usize]
            ^ X_TIME_3_S_BOX[state[10] as usize]
            ^ S_BOX[state[15] as usize]),
        (S_BOX[state[0] as usize]
            ^ S_BOX[state[5] as usize]
            ^ X_TIME_2_S_BOX[state[10] as usize]
            ^ X_TIME_3_S_BOX[state[15] as usize]),
        (X_TIME_3_S_BOX[state[0] as usize]
            ^ S_BOX[state[5] as usize]
            ^ S_BOX[state[10] as usize]
            ^ X_TIME_2_S_BOX[state[15] as usize]),
        // mixing column 1
        (X_TIME_2_S_BOX[state[4] as usize]
            ^ X_TIME_3_S_BOX[state[9] as usize]
            ^ S_BOX[state[14] as usize]
            ^ S_BOX[state[3] as usize]),
        (S_BOX[state[4] as usize]
            ^ X_TIME_2_S_BOX[state[9] as usize]
            ^ X_TIME_3_S_BOX[state[14] as usize]
            ^ S_BOX[state[3] as usize]),
        (S_BOX[state[4] as usize]
            ^ S_BOX[state[9] as usize]
            ^ X_TIME_2_S_BOX[state[14] as usize]
            ^ X_TIME_3_S_BOX[state[3] as usize]),
        (X_TIME_3_S_BOX[state[4] as usize]
            ^ S_BOX[state[9] as usize]
            ^ S_BOX[state[14] as usize]
            ^ X_TIME_2_S_BOX[state[3] as usize]),
        // mixing column 2
        (X_TIME_2_S_BOX[state[8] as usize]
            ^ X_TIME_3_S_BOX[state[13] as usize]
            ^ S_BOX[state[2] as usize]
            ^ S_BOX[state[7] as usize]),
        (S_BOX[state[8] as usize]
            ^ X_TIME_2_S_BOX[state[13] as usize]
            ^ X_TIME_3_S_BOX[state[2] as usize]
            ^ S_BOX[state[7] as usize]),
        (S_BOX[state[8] as usize]
            ^ S_BOX[state[13] as usize]
            ^ X_TIME_2_S_BOX[state[2] as usize]
            ^ X_TIME_3_S_BOX[state[7] as usize]),
        (X_TIME_3_S_BOX[state[8] as usize]
            ^ S_BOX[state[13] as usize]
            ^ S_BOX[state[2] as usize]
            ^ X_TIME_2_S_BOX[state[7] as usize]),
        // mixing column 3
        (X_TIME_2_S_BOX[state[12] as usize]
            ^ X_TIME_3_S_BOX[state[1] as usize]
            ^ S_BOX[state[6] as usize]
            ^ S_BOX[state[11] as usize]),
        (S_BOX[state[12] as usize]
            ^ X_TIME_2_S_BOX[state[1] as usize]
            ^ X_TIME_3_S_BOX[state[6] as usize]
            ^ S_BOX[state[11] as usize]),
        (S_BOX[state[12] as usize]
            ^ S_BOX[state[1] as usize]
            ^ X_TIME_2_S_BOX[state[6] as usize]
            ^ X_TIME_3_S_BOX[state[11] as usize]),
        (X_TIME_3_S_BOX[state[12] as usize]
            ^ S_BOX[state[1] as usize]
            ^ S_BOX[state[6] as usize]
            ^ X_TIME_2_S_BOX[state[11] as usize]),
    ];

    state.copy_from_slice(&tmp);
}

/// Combine all three inverse steps of AES:
/// Inverse SubBytes
/// Inverse ShiftRows
/// Inverse MixColumns
pub fn inverse_round_operation(state: &mut [u8]) {
    let mut tmp = [0u8; 16];

    // mixing column 0
    tmp[0] = X_TIME_E[state[0] as usize]
        ^ X_TIME_B[state[1] as usize]
        ^ X_TIME_D[state[2] as usize]
        ^ X_TIME_9[state[3] as usize];
    tmp[5] = X_TIME_9[state[0] as usize]
        ^ X_TIME_E[state[1] as usize]
        ^ X_TIME_B[state[2] as usize]
        ^ X_TIME_D[state[3] as usize];
    tmp[10] = X_TIME_D[state[0] as usize]
        ^ X_TIME_9[state[1] as usize]
        ^ X_TIME_E[state[2] as usize]
        ^ X_TIME_B[state[3] as usize];
    tmp[15] = X_TIME_B[state[0] as usize]
        ^ X_TIME_D[state[1] as usize]
        ^ X_TIME_9[state[2] as usize]
        ^ X_TIME_E[state[3] as usize];

    // mixing column 1
    tmp[4] = X_TIME_E[state[4] as usize]
        ^ X_TIME_B[state[5] as usize]
        ^ X_TIME_D[state[6] as usize]
        ^ X_TIME_9[state[7] as usize];
    tmp[9] = X_TIME_9[state[4] as usize]
        ^ X_TIME_E[state[5] as usize]
        ^ X_TIME_B[state[6] as usize]
        ^ X_TIME_D[state[7] as usize];
    tmp[14] = X_TIME_D[state[4] as usize]
        ^ X_TIME_9[state[5] as usize]
        ^ X_TIME_E[state[6] as usize]
        ^ X_TIME_B[state[7] as usize];
    tmp[3] = X_TIME_B[state[4] as usize]
        ^ X_TIME_D[state[5] as usize]
        ^ X_TIME_9[state[6] as usize]
        ^ X_TIME_E[state[7] as usize];

    // mixing column 2
    tmp[8] = X_TIME_E[state[8] as usize]
        ^ X_TIME_B[state[9] as usize]
        ^ X_TIME_D[state[10] as usize]
        ^ X_TIME_9[state[11] as usize];
    tmp[13] = X_TIME_9[state[8] as usize]
        ^ X_TIME_E[state[9] as usize]
        ^ X_TIME_B[state[10] as usize]
        ^ X_TIME_D[state[11] as usize];
    tmp[2] = X_TIME_D[state[8] as usize]
        ^ X_TIME_9[state[9] as usize]
        ^ X_TIME_E[state[10] as usize]
        ^ X_TIME_B[state[11] as usize];
    tmp[7] = X_TIME_B[state[8] as usize]
        ^ X_TIME_D[state[9] as usize]
        ^ X_TIME_9[state[10] as usize]
        ^ X_TIME_E[state[11] as usize];

    // mixing column 3
    tmp[12] = X_TIME_E[state[12] as usize]
        ^ X_TIME_B[state[13] as usize]
        ^ X_TIME_D[state[14] as usize]
        ^ X_TIME_9[state[15] as usize];
    tmp[1] = X_TIME_9[state[12] as usize]
        ^ X_TIME_E[state[13] as usize]
        ^ X_TIME_B[state[14] as usize]
        ^ X_TIME_D[state[15] as usize];
    tmp[6] = X_TIME_D[state[12] as usize]
        ^ X_TIME_9[state[13] as usize]
        ^ X_TIME_E[state[14] as usize]
        ^ X_TIME_B[state[15] as usize];
    tmp[11] = X_TIME_B[state[12] as usize]
        ^ X_TIME_D[state[13] as usize]
        ^ X_TIME_9[state[14] as usize]
        ^ X_TIME_E[state[15] as usize];

    for i in 0..16 {
        state[i] = INVERSE_S_BOX[tmp[i] as usize];
    }
}

/// Round constants for key expansion. The first 0 is never used and could be anything.
const ROUND_CONSTANTS: [u8; 11] = [
    0x00, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36,
];

/// Return the full round key according to the Rijndael key expansion algorithm.
pub fn key_expansion(mut key: Vec<u8>) -> Vec<u8> {
    // n is the length of the key in 4-bytes words: 4 for AES-128, 6 for AES-192, and 8 for AES-256
    let n = key.len() / 4;
    let r = n + 7;
    key.resize(16 * r, 0);

    for i in n..4 * r {
        let mut prev = key[4 * (i - 1)..4 * i].to_vec();
        let win = key[4 * (i - n)..4 * (i - n + 1)].to_vec(); // W_(i-n)

        if i % n == 0 {
            prev.rotate_left(1);
            sub_bytes(&mut prev);
            prev[0] ^= ROUND_CONSTANTS[i / n];
        } else if n == 8 && i % n == 4 {
            sub_bytes(&mut prev);
        }

        key[4 * i] = prev[0] ^ win[0];
        key[4 * i + 1] = prev[1] ^ win[1];
        key[4 * i + 2] = prev[2] ^ win[2];
        key[4 * i + 3] = prev[3] ^ win[3];
    }
    key
}

/// Encrypt a 16-bytes block with given expanded key using AES.
/// The optional third argument is the 16-bytes long initialization vector.
pub fn encrypt_block(key: &[u8], block: &mut [u8], init_vec: Option<&[u8]>) {
    let add_round_key = |block: &mut [u8], n: usize| {
        for (i, e) in block.iter_mut().enumerate() {
            *e ^= key[i + (n << 4)];
        }
    };
    match init_vec {
        Some(v) => {
            for (i, e) in block.iter_mut().enumerate() {
                // combine add_round_key(0) and xor init_vec
                *e ^= key[i] ^ v[i];
            }
        }
        None => add_round_key(block, 0),
    }
    let n = (key.len() >> 4) - 1;

    for i in 1..n {
        round_operation(block);
        add_round_key(block, i);
    }
    box_shift_rows(block);
    add_round_key(block, n);
}

/// Decrypt a 16-bytes block with given expanded key using AES.
/// The optional third argument is the 16-bytes long initialization vector.
pub fn decrypt_block(key: &[u8], block: &mut [u8], init_vec: Option<&[u8]>) {
    let add_round_key = |block: &mut [u8], n: usize| {
        for (i, e) in block.iter_mut().enumerate() {
            *e ^= key[i + (n << 4)];
        }
    };
    let n = (key.len() >> 4) - 1;

    add_round_key(block, n);
    inverse_box_shift_rows(block);
    for i in (1..n).rev() {
        add_round_key(block, i);
        inverse_round_operation(block);
    }
    add_round_key(block, 0);

    if let Some(v) = init_vec {
        for (i, e) in block.iter_mut().enumerate() {
            *e ^= v[i];
        }
    }
}

/// Encrypt given plaintext using AES in ECB mode.
pub fn encrypt_ecb(key: Vec<u8>, text: &mut Vec<u8>) {
    let big_key = key_expansion(key);
    utils::pad_pkcs7(text, 16);
    for i in 0..(text.len() >> 4) {
        encrypt_block(&big_key, &mut text[(i << 4)..((i + 1) << 4)], None);
    }
}

/// Decrypt given ciphertext using AES in ECB mode.
pub fn decrypt_ecb(key: Vec<u8>, text: &mut Vec<u8>) {
    let big_key = key_expansion(key);
    for i in 0..(text.len() >> 4) {
        decrypt_block(&big_key, &mut text[(i << 4)..((i + 1) << 4)], None);
    }
    utils::unpad_pkcs7(text)
}

/// Encrypt given plaintext using AES in CBC mode with given initialization vector.
pub fn encrypt_cbc(key: Vec<u8>, text: &mut Vec<u8>, init_vec: &[u8]) {
    let big_key = key_expansion(key);
    utils::pad_pkcs7(text, 16);
    let mut previous: Vec<u8> = init_vec.to_owned();
    for i in 0..(text.len() >> 4) {
        encrypt_block(
            &big_key,
            &mut text[(i << 4)..((i + 1) << 4)],
            Some(&previous),
        );
        previous.copy_from_slice(&text[(i << 4)..((i + 1) << 4)]);
    }
}

/// Decrypt given ciphertext using AES in CBC mode with given initialization vector.
pub fn decrypt_cbc(key: Vec<u8>, text: &mut Vec<u8>, init_vec: &[u8], unpad: bool) {
    let big_key = key_expansion(key);
    let mut previous: Vec<u8> = init_vec.to_owned();
    for i in 0..(text.len() >> 4) {
        let tmp = &text[(i << 4)..((i + 1) << 4)].to_owned();
        decrypt_block(
            &big_key,
            &mut text[(i << 4)..((i + 1) << 4)],
            Some(&previous),
        );
        previous.copy_from_slice(tmp);
    }
    if unpad {
        utils::unpad_pkcs7(text)
    }
}

/// One of the lines in given file has been encrypted with ECB.
/// This function finds that line and returns its index in the file.
pub fn detect_ecb(file_name: &str) -> usize {
    let s = utils::read_file(file_name, false);
    for (i, line) in s.lines().enumerate() {
        let mut s = std::collections::HashSet::new();
        for j in 0..10 {
            s.insert(&line[(j << 5)..((j + 1) << 5)]);
        }
        if s.len() != 10 {
            return i + 1;
        }
    }
    panic!("Couldn't find a line with duplicates");
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn key_expansion_test() {
        let k = vec![0; 16];
        let res = key_expansion(k);
        let should_be = vec![
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x62, 0x63, 0x63, 0x63, 0x62, 0x63, 0x63, 0x63, 0x62, 0x63, 0x63, 0x63,
            0x62, 0x63, 0x63, 0x63, 0x9b, 0x98, 0x98, 0xc9, 0xf9, 0xfb, 0xfb, 0xaa, 0x9b, 0x98,
            0x98, 0xc9, 0xf9, 0xfb, 0xfb, 0xaa, 0x90, 0x97, 0x34, 0x50, 0x69, 0x6c, 0xcf, 0xfa,
            0xf2, 0xf4, 0x57, 0x33, 0x0b, 0x0f, 0xac, 0x99, 0xee, 0x06, 0xda, 0x7b, 0x87, 0x6a,
            0x15, 0x81, 0x75, 0x9e, 0x42, 0xb2, 0x7e, 0x91, 0xee, 0x2b, 0x7f, 0x2e, 0x2b, 0x88,
            0xf8, 0x44, 0x3e, 0x09, 0x8d, 0xda, 0x7c, 0xbb, 0xf3, 0x4b, 0x92, 0x90, 0xec, 0x61,
            0x4b, 0x85, 0x14, 0x25, 0x75, 0x8c, 0x99, 0xff, 0x09, 0x37, 0x6a, 0xb4, 0x9b, 0xa7,
            0x21, 0x75, 0x17, 0x87, 0x35, 0x50, 0x62, 0x0b, 0xac, 0xaf, 0x6b, 0x3c, 0xc6, 0x1b,
            0xf0, 0x9b, 0x0e, 0xf9, 0x03, 0x33, 0x3b, 0xa9, 0x61, 0x38, 0x97, 0x06, 0x0a, 0x04,
            0x51, 0x1d, 0xfa, 0x9f, 0xb1, 0xd4, 0xd8, 0xe2, 0x8a, 0x7d, 0xb9, 0xda, 0x1d, 0x7b,
            0xb3, 0xde, 0x4c, 0x66, 0x49, 0x41, 0xb4, 0xef, 0x5b, 0xcb, 0x3e, 0x92, 0xe2, 0x11,
            0x23, 0xe9, 0x51, 0xcf, 0x6f, 0x8f, 0x18, 0x8e,
        ];
        assert_eq!(res, should_be);

        let k = vec![255; 16];
        let res = key_expansion(k);
        let should_be = vec![
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
            0xff, 0xff, 0xe8, 0xe9, 0xe9, 0xe9, 0x17, 0x16, 0x16, 0x16, 0xe8, 0xe9, 0xe9, 0xe9,
            0x17, 0x16, 0x16, 0x16, 0xad, 0xae, 0xae, 0x19, 0xba, 0xb8, 0xb8, 0x0f, 0x52, 0x51,
            0x51, 0xe6, 0x45, 0x47, 0x47, 0xf0, 0x09, 0x0e, 0x22, 0x77, 0xb3, 0xb6, 0x9a, 0x78,
            0xe1, 0xe7, 0xcb, 0x9e, 0xa4, 0xa0, 0x8c, 0x6e, 0xe1, 0x6a, 0xbd, 0x3e, 0x52, 0xdc,
            0x27, 0x46, 0xb3, 0x3b, 0xec, 0xd8, 0x17, 0x9b, 0x60, 0xb6, 0xe5, 0xba, 0xf3, 0xce,
            0xb7, 0x66, 0xd4, 0x88, 0x04, 0x5d, 0x38, 0x50, 0x13, 0xc6, 0x58, 0xe6, 0x71, 0xd0,
            0x7d, 0xb3, 0xc6, 0xb6, 0xa9, 0x3b, 0xc2, 0xeb, 0x91, 0x6b, 0xd1, 0x2d, 0xc9, 0x8d,
            0xe9, 0x0d, 0x20, 0x8d, 0x2f, 0xbb, 0x89, 0xb6, 0xed, 0x50, 0x18, 0xdd, 0x3c, 0x7d,
            0xd1, 0x50, 0x96, 0x33, 0x73, 0x66, 0xb9, 0x88, 0xfa, 0xd0, 0x54, 0xd8, 0xe2, 0x0d,
            0x68, 0xa5, 0x33, 0x5d, 0x8b, 0xf0, 0x3f, 0x23, 0x32, 0x78, 0xc5, 0xf3, 0x66, 0xa0,
            0x27, 0xfe, 0x0e, 0x05, 0x14, 0xa3, 0xd6, 0x0a, 0x35, 0x88, 0xe4, 0x72, 0xf0, 0x7b,
            0x82, 0xd2, 0xd7, 0x85, 0x8c, 0xd7, 0xc3, 0x26,
        ];
        assert_eq!(res, should_be);

        let k = (0..16).collect();
        let res = key_expansion(k);
        let should_be = vec![
            0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d,
            0x0e, 0x0f, 0xd6, 0xaa, 0x74, 0xfd, 0xd2, 0xaf, 0x72, 0xfa, 0xda, 0xa6, 0x78, 0xf1,
            0xd6, 0xab, 0x76, 0xfe, 0xb6, 0x92, 0xcf, 0x0b, 0x64, 0x3d, 0xbd, 0xf1, 0xbe, 0x9b,
            0xc5, 0x00, 0x68, 0x30, 0xb3, 0xfe, 0xb6, 0xff, 0x74, 0x4e, 0xd2, 0xc2, 0xc9, 0xbf,
            0x6c, 0x59, 0x0c, 0xbf, 0x04, 0x69, 0xbf, 0x41, 0x47, 0xf7, 0xf7, 0xbc, 0x95, 0x35,
            0x3e, 0x03, 0xf9, 0x6c, 0x32, 0xbc, 0xfd, 0x05, 0x8d, 0xfd, 0x3c, 0xaa, 0xa3, 0xe8,
            0xa9, 0x9f, 0x9d, 0xeb, 0x50, 0xf3, 0xaf, 0x57, 0xad, 0xf6, 0x22, 0xaa, 0x5e, 0x39,
            0x0f, 0x7d, 0xf7, 0xa6, 0x92, 0x96, 0xa7, 0x55, 0x3d, 0xc1, 0x0a, 0xa3, 0x1f, 0x6b,
            0x14, 0xf9, 0x70, 0x1a, 0xe3, 0x5f, 0xe2, 0x8c, 0x44, 0x0a, 0xdf, 0x4d, 0x4e, 0xa9,
            0xc0, 0x26, 0x47, 0x43, 0x87, 0x35, 0xa4, 0x1c, 0x65, 0xb9, 0xe0, 0x16, 0xba, 0xf4,
            0xae, 0xbf, 0x7a, 0xd2, 0x54, 0x99, 0x32, 0xd1, 0xf0, 0x85, 0x57, 0x68, 0x10, 0x93,
            0xed, 0x9c, 0xbe, 0x2c, 0x97, 0x4e, 0x13, 0x11, 0x1d, 0x7f, 0xe3, 0x94, 0x4a, 0x17,
            0xf3, 0x07, 0xa7, 0x8b, 0x4d, 0x2b, 0x30, 0xc5,
        ];
        assert_eq!(res, should_be);

        let k = (0..24).collect();
        let res = key_expansion(k);
        let should_be = vec![
            0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d,
            0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x58, 0x46, 0xf2, 0xf9,
            0x5c, 0x43, 0xf4, 0xfe, 0x54, 0x4a, 0xfe, 0xf5, 0x58, 0x47, 0xf0, 0xfa, 0x48, 0x56,
            0xe2, 0xe9, 0x5c, 0x43, 0xf4, 0xfe, 0x40, 0xf9, 0x49, 0xb3, 0x1c, 0xba, 0xbd, 0x4d,
            0x48, 0xf0, 0x43, 0xb8, 0x10, 0xb7, 0xb3, 0x42, 0x58, 0xe1, 0x51, 0xab, 0x04, 0xa2,
            0xa5, 0x55, 0x7e, 0xff, 0xb5, 0x41, 0x62, 0x45, 0x08, 0x0c, 0x2a, 0xb5, 0x4b, 0xb4,
            0x3a, 0x02, 0xf8, 0xf6, 0x62, 0xe3, 0xa9, 0x5d, 0x66, 0x41, 0x0c, 0x08, 0xf5, 0x01,
            0x85, 0x72, 0x97, 0x44, 0x8d, 0x7e, 0xbd, 0xf1, 0xc6, 0xca, 0x87, 0xf3, 0x3e, 0x3c,
            0xe5, 0x10, 0x97, 0x61, 0x83, 0x51, 0x9b, 0x69, 0x34, 0x15, 0x7c, 0x9e, 0xa3, 0x51,
            0xf1, 0xe0, 0x1e, 0xa0, 0x37, 0x2a, 0x99, 0x53, 0x09, 0x16, 0x7c, 0x43, 0x9e, 0x77,
            0xff, 0x12, 0x05, 0x1e, 0xdd, 0x7e, 0x0e, 0x88, 0x7e, 0x2f, 0xff, 0x68, 0x60, 0x8f,
            0xc8, 0x42, 0xf9, 0xdc, 0xc1, 0x54, 0x85, 0x9f, 0x5f, 0x23, 0x7a, 0x8d, 0x5a, 0x3d,
            0xc0, 0xc0, 0x29, 0x52, 0xbe, 0xef, 0xd6, 0x3a, 0xde, 0x60, 0x1e, 0x78, 0x27, 0xbc,
            0xdf, 0x2c, 0xa2, 0x23, 0x80, 0x0f, 0xd8, 0xae, 0xda, 0x32, 0xa4, 0x97, 0x0a, 0x33,
            0x1a, 0x78, 0xdc, 0x09, 0xc4, 0x18, 0xc2, 0x71, 0xe3, 0xa4, 0x1d, 0x5d,
        ];
        assert_eq!(res, should_be);

        let k = (0..32).collect();
        let res = key_expansion(k);
        let should_be = vec![
            0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d,
            0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b,
            0x1c, 0x1d, 0x1e, 0x1f, 0xa5, 0x73, 0xc2, 0x9f, 0xa1, 0x76, 0xc4, 0x98, 0xa9, 0x7f,
            0xce, 0x93, 0xa5, 0x72, 0xc0, 0x9c, 0x16, 0x51, 0xa8, 0xcd, 0x02, 0x44, 0xbe, 0xda,
            0x1a, 0x5d, 0xa4, 0xc1, 0x06, 0x40, 0xba, 0xde, 0xae, 0x87, 0xdf, 0xf0, 0x0f, 0xf1,
            0x1b, 0x68, 0xa6, 0x8e, 0xd5, 0xfb, 0x03, 0xfc, 0x15, 0x67, 0x6d, 0xe1, 0xf1, 0x48,
            0x6f, 0xa5, 0x4f, 0x92, 0x75, 0xf8, 0xeb, 0x53, 0x73, 0xb8, 0x51, 0x8d, 0xc6, 0x56,
            0x82, 0x7f, 0xc9, 0xa7, 0x99, 0x17, 0x6f, 0x29, 0x4c, 0xec, 0x6c, 0xd5, 0x59, 0x8b,
            0x3d, 0xe2, 0x3a, 0x75, 0x52, 0x47, 0x75, 0xe7, 0x27, 0xbf, 0x9e, 0xb4, 0x54, 0x07,
            0xcf, 0x39, 0x0b, 0xdc, 0x90, 0x5f, 0xc2, 0x7b, 0x09, 0x48, 0xad, 0x52, 0x45, 0xa4,
            0xc1, 0x87, 0x1c, 0x2f, 0x45, 0xf5, 0xa6, 0x60, 0x17, 0xb2, 0xd3, 0x87, 0x30, 0x0d,
            0x4d, 0x33, 0x64, 0x0a, 0x82, 0x0a, 0x7c, 0xcf, 0xf7, 0x1c, 0xbe, 0xb4, 0xfe, 0x54,
            0x13, 0xe6, 0xbb, 0xf0, 0xd2, 0x61, 0xa7, 0xdf, 0xf0, 0x1a, 0xfa, 0xfe, 0xe7, 0xa8,
            0x29, 0x79, 0xd7, 0xa5, 0x64, 0x4a, 0xb3, 0xaf, 0xe6, 0x40, 0x25, 0x41, 0xfe, 0x71,
            0x9b, 0xf5, 0x00, 0x25, 0x88, 0x13, 0xbb, 0xd5, 0x5a, 0x72, 0x1c, 0x0a, 0x4e, 0x5a,
            0x66, 0x99, 0xa9, 0xf2, 0x4f, 0xe0, 0x7e, 0x57, 0x2b, 0xaa, 0xcd, 0xf8, 0xcd, 0xea,
            0x24, 0xfc, 0x79, 0xcc, 0xbf, 0x09, 0x79, 0xe9, 0x37, 0x1a, 0xc2, 0x3c, 0x6d, 0x68,
            0xde, 0x36,
        ];
        assert_eq!(res, should_be);
    }

    #[test]
    fn cbc_padding_test() {
        let key = vec![0; 16];
        let iv = [0; 16];
        let mut text3 = vec![0; 3];
        encrypt_cbc(key.clone(), &mut text3, &iv);
        assert_eq!(text3.len(), 16);

        let mut text8 = vec![0; 8];
        encrypt_cbc(key.clone(), &mut text8, &iv);
        assert_eq!(text8.len(), 16);

        let mut text16 = vec![0; 16];
        encrypt_cbc(key.clone(), &mut text16, &iv);
        assert_eq!(text16.len(), 16);

        let mut text17 = vec![0; 17];
        encrypt_cbc(key.clone(), &mut text17, &iv);
        assert_eq!(text17.len(), 32);
    }
}
