mod aes;
mod block_cipher_crypto;
mod challenges;
mod conversions;
mod utils;
mod xor_crypto;
use std::env;

fn main() {
    let args: Vec<_> = env::args().collect();
    let err_msg = "Please enter either 'chal[lenge]' or 'set' followed by a number.";
    if args.len() < 3 {
        println!("{}", err_msg);
        return;
    }
    match (args[1].as_ref(), args[2].parse::<usize>()) {
        (_, Err(_)) => println!("{}", err_msg),
        ("set", Ok(n @ 1..=challenges::NB_SETS)) => challenges::set(n),
        ("set", Ok(_)) => println!("I haven't implemented that set yet!"),
        ("challenge", Ok(n @ 1..=challenges::NB_CHALLENGES))
        | ("chal", Ok(n @ 1..=challenges::NB_CHALLENGES)) => challenges::challenge(n),
        ("challenge", Ok(_)) | ("chal", Ok(_)) => {
            println!("I haven't implemented that challenge yet!")
        }
        _ => println!("{}", err_msg),
    }
}
