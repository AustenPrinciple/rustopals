use crate::aes;
use crate::block_cipher_crypto as bcc;
use crate::conversions as cv;
use crate::utils;
use crate::xor_crypto as xc;
use std::{cmp, thread};

pub const SET_SIZE: usize = 8;
pub const NB_CHALLENGES: usize = FUNCTIONS.len();
pub const NB_SETS: usize = (NB_CHALLENGES - 1) * SET_SIZE + 1;

const FUNCTIONS: [&(dyn Fn() -> String + Sync); 17] = [
    &challenge1,
    &challenge2,
    &challenge3,
    &challenge4,
    &challenge5,
    &challenge6,
    &challenge7,
    &challenge8,
    &challenge9,
    &challenge10,
    &challenge11,
    &challenge12,
    &challenge13,
    &challenge14,
    &challenge15,
    &challenge16,
    &challenge17,
];

fn challenge1() -> String {
    let hex = String::from("49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d");
    cv::hex_to_64(hex)
}

fn challenge2() -> String {
    let hex1 = String::from("1c0111001f010100061a024b53535009181c");
    let hex2 = String::from("686974207468652062756c6c277320657965");
    let b1 = cv::hex_to_bytes(hex1);
    let b2 = cv::hex_to_bytes(hex2);
    b1.iter()
        .zip(b2)
        .map(|(l, r)| format!("{:02x}", l ^ r))
        .collect()
}

fn challenge3() -> String {
    let hex = String::from("1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736");
    let b1 = cv::hex_to_bytes(hex);
    let (b2, _) = xc::break_single_xor(&b1);
    cv::bytes_to_text(&b2)
}

fn challenge4() -> String {
    let s = utils::read_file("../cryptopals/resources/data1-4.txt", false);
    let b = xc::detect_single_xor(s);
    cv::bytes_to_text(&b)
}

fn challenge5() -> String {
    let key = cv::text_to_bytes(String::from("ICE"));
    let text = cv::text_to_bytes(String::from(
        "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal",
    ));
    cv::bytes_to_hex(&xc::repeating_xor(&key, &text))
}

fn challenge6() -> String {
    let (text, key) = xc::break_repeating_xor(&cv::base64_to_bytes(utils::read_file(
        "../cryptopals/resources/data1-6.txt",
        true,
    )));
    format!(
        "Key = \"{}\" and Text =\n{}",
        cv::bytes_to_text(&key),
        cv::bytes_to_text(&text)
    )
}

fn challenge7() -> String {
    let mut input = cv::base64_to_bytes(utils::read_file(
        "../cryptopals/resources/data1-7.txt",
        true,
    ));
    aes::decrypt_ecb(
        cv::text_to_bytes(String::from("YELLOW SUBMARINE")),
        &mut input,
    );
    cv::bytes_to_text(&input)
}

fn challenge8() -> String {
    aes::detect_ecb("../cryptopals/resources/data1-8.txt").to_string()
}

fn challenge9() -> String {
    let mut v = cv::text_to_bytes(String::from("YELLOW SUBMARINE"));
    utils::pad_pkcs7(&mut v, 20);
    cv::bytes_to_text(&v)
}

fn challenge10() -> String {
    let key = cv::text_to_bytes(String::from("YELLOW SUBMARINE"));
    let mut input = cv::base64_to_bytes(utils::read_file(
        "../cryptopals/resources/data2-2.txt",
        true,
    ));
    let iv: [u8; 16] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    aes::decrypt_cbc(key, &mut input, &iv, true);
    cv::bytes_to_text(&input)
}

fn challenge11() -> String {
    match (0..100).try_for_each(|_| bcc::predict_oracle(&bcc::encrypt_oracle)) {
        Ok(()) => String::from("Successfully found the mode the box was using."),
        Err(s) => s,
    }
}

fn challenge12() -> String {
    String::from_utf8(bcc::break_ecb(false)).unwrap()
}

fn challenge13() -> String {
    let m = bcc::break_profile();
    format!(
        "{{\n\temail: '{}',\n\tuid: {},\n\trole: '{}',\n}}",
        m.get("email").unwrap(),
        m.get("uid").unwrap(),
        m.get("role").unwrap()
    )
}

fn challenge14() -> String {
    String::from_utf8(bcc::break_ecb(true)).unwrap()
}

fn challenge15() -> String {
    format!(
        "{}\n{}",
        "The function asked in this challenge is in utils:",
        "'validate-pkcs7', based on 'unpad-pkcs7'"
    )
}

fn challenge16() -> String {
    bcc::decrypt_comments(bcc::break_comments())
}

fn challenge17() -> String {
    let (text, iv) = bcc::crypt_line();
    let b = bcc::check_padding(text, iv);
    format!("Oracle said {}", b)
}

pub fn challenge(n: usize) {
    println!("{}", FUNCTIONS[n - 1]());
}

pub fn set(n: usize) {
    let mut handles = Vec::with_capacity(SET_SIZE);
    let start_index = (n - 1) * SET_SIZE;
    // start all the jobs in separate threads
    for f in &FUNCTIONS[start_index..cmp::min(start_index + SET_SIZE, NB_CHALLENGES)] {
        handles.push(thread::spawn(f));
    }
    // collect the results
    for (i, h) in handles.into_iter().enumerate() {
        println!(
            "--------------------------------------------------------------------------------"
        );
        let s = h.join().unwrap();
        println!("Challenge {}:\n{}", start_index + i + 1, s);
    }
}
