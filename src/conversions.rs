extern crate base64;
extern crate hex;
use crate::utils;

/// Convert a byte sequence into the number it represents, in hexadecimal.
pub fn bytes_to_hex(v: &[u8]) -> String {
    v.iter().map(|n: &u8| format!("{:02x}", n)).collect()
}

/// Convert an hexadecimal string into a byte sequence.
pub fn hex_to_bytes(mut s: String) -> Vec<u8> {
    utils::pad_bin(&mut s, 2, false);
    hex::decode(s).unwrap()
}

/// Decode byte sequence 's' as text string.
pub fn bytes_to_text(s: &[u8]) -> String {
    String::from_utf8(s.to_vec()).unwrap()
}

/// Encode text string as byte sequence.
pub fn text_to_bytes(s: String) -> Vec<u8> {
    s.into_bytes()
}

/// Decode base64 text string as byte sequence.
pub fn base64_to_bytes(s: String) -> Vec<u8> {
    base64::decode(s).unwrap()
}

/// Convert a hex-encoded number into a string representing that number in base64.
pub fn hex_to_64(s: String) -> String {
    let hex = hex::decode(s).unwrap();
    base64::encode(hex)
}
