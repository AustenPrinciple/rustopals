use crate::aes;
use crate::conversions as cv;
use crate::utils;
use rand::{thread_rng, Rng};
use std::collections::HashMap;

const RANDOM_KEY: [u8; 16] = [
    35, 84, 196, 89, 86, 0, 136, 158, 240, 214, 188, 180, 202, 85, 34, 211,
];

const RANDOM_PREFIX: [u8; 20] = [
    230, 129, 250, 100, 116, 127, 69, 91, 145, 153, 12, 27, 236, 85, 34, 211, 200, 2, 100, 8,
];

/// Encrypt text using, at random, AES in CBC or ECB mode, with a random key.
/// In CBC mode, it will also use a random initialization vector.
/// It will also sneakily add useless bytes before and after the ciphertext.
pub fn encrypt_oracle(text: &mut Vec<u8>) -> String {
    let mut gen = thread_rng();
    let key = utils::random_bytes(16, Some(&mut gen));
    let mut bytes_before = utils::random_bytes(gen.gen_range(5..=10), Some(&mut gen));
    let mut bytes_after = utils::random_bytes(gen.gen_range(5..=10), Some(&mut gen));
    text.append(&mut bytes_before);
    text.rotate_right(bytes_before.len());
    text.append(&mut bytes_after);
    if gen.gen() {
        aes::encrypt_ecb(key, text);
        String::from("ECB")
    } else {
        aes::encrypt_cbc(key, text, &utils::random_bytes(16, Some(&mut gen)));
        String::from("CBC")
    }
}

/// When given a function that might be using ECB or CBC mode, detect which one is in use.
pub fn predict_oracle(black_box: &dyn Fn(&mut Vec<u8>) -> String) -> Result<(), String> {
    let some_number = rand::random();
    let mut text = vec![some_number; 256];
    let used_mode = black_box(&mut text);
    let mut dupes = 0;
    for i in 0..240 {
        if text[i] == text[i + 16] {
            dupes += 1;
        }
    }
    let inferred_mode = if dupes > 200 { "ECB" } else { "CBC" };
    if used_mode == inferred_mode {
        Ok(())
    } else {
        Err(format!(
            "Couldn't find the mode the box was using. (dupes: {})",
            dupes
        ))
    }
}

/// Encrypt given text after appending a long string to it.
/// We're trying to decode that very string.
/// When second argument is true, add above random prefix to your text, to make decryption harder.
/// This also changes the coded text to something else, to spice things up a bit.
fn help_break(attack_text: &mut Vec<u8>, with_prefix: bool) {
    let file = if !with_prefix {
        "../cryptopals/resources/data2-4.txt"
    } else {
        "../cryptopals/resources/data2-6.txt"
    };
    let mut target_text = cv::base64_to_bytes(utils::read_file(file, true));
    if with_prefix {
        attack_text.append(&mut RANDOM_PREFIX[..].to_owned());
        attack_text.rotate_right(RANDOM_PREFIX.len());
    }
    attack_text.append(&mut target_text);
    aes::encrypt_ecb(RANDOM_KEY.to_vec(), attack_text);
}

/// Find the total size of the ciphertext+prefix and the block size the black box
/// function is using. It would be 16 for everything related to AES.
fn find_sizes(box_fn: &dyn Fn(&mut Vec<u8>)) -> (usize, usize) {
    let mut dummy_vec = vec![];
    box_fn(&mut dummy_vec);
    let init_size = dummy_vec.len();
    for n in 1.. {
        let mut tester = vec![0u8; n];
        box_fn(&mut tester);
        let test_size = tester.len();
        if init_size != test_size {
            return (init_size - n + 1, test_size - init_size);
        }
    }
    panic!("Couldn't find sizes!")
}

/// Find first non-prefix line:
/// Given a black-box function that adds an unknown-sized-prefix to our text and
/// the block size of that function, find the index of the first line of any
/// 'boxed' text that does not contain any byte from the prefix.
/// This is done by sending 3 times the block size worth of 0-bytes, and finding
/// duplicate lines in the result ciphertext.
/// The index we are looking for will then be the index of the first twin line.
fn find_np_line(box_fn: &dyn Fn(&mut Vec<u8>), size: usize) -> usize {
    let mut lines = vec![0u8; 3 * size];
    box_fn(&mut lines);
    let mut iterator = lines.chunks(size).peekable();
    for i in 0.. {
        let l = iterator.next().unwrap();
        let &next = iterator.peek().unwrap();
        if l == next {
            return i;
        }
    }
    panic!("Couldn't find non-prefix line!")
}

/// Given a `black-box` function that adds an unknown-sized-prefix to our text,
/// its block `size`, and the index calculated above, find the length of the last
/// line of the prefix.
/// This is done by first sending 2 times the block size worth of 0-bytes,
/// then 2 times minus 2, then 2 times minus 3, etc. When we find a line different
/// from the initial one, we have found the size we were looking for.
/// The reason we start at 2 on the loop (instead of 0 or 1) is quite subtle:
/// Since we look for differences and we want to return the last value without
/// differences, at the end of the loop we return i-1. That excludes starting
/// at 0. The only case where we would return 0 is when there is no prefix, which
/// is already tested at the start of the function. We therefore don't need to
/// check again, and we can start at 2 (minimum return value in the loop is 1).
fn find_prefix_size(box_fn: &dyn Fn(&mut Vec<u8>), size: usize, np_line: usize) -> usize {
    if np_line == 0 {
        return 0;
    }
    let mut null_vec = vec![0u8; 2 * size];
    box_fn(&mut null_vec);
    let null_line = &null_vec[(np_line * size)..((np_line + 1) * size)];
    for i in 2.. {
        let mut test_vec = vec![0u8; 2 * size - i];
        box_fn(&mut test_vec);
        let test_line = &test_vec[(np_line * size)..((np_line + 1) * size)];
        if *null_line != *test_line {
            return i - 1;
        }
    }
    panic!("Couldn't find prefix size!")
}

/// Break ecb with or without a prefix.
pub fn break_ecb(with_prefix: bool) -> Vec<u8> {
    let cipher_box = |x: &mut Vec<u8>| help_break(x, with_prefix);
    // Here temp_size is the size of prefix+text
    let (temp_size, size) = find_sizes(&cipher_box);
    let first_np_line = find_np_line(&cipher_box, size);
    let prefix_size = find_prefix_size(&cipher_box, size, first_np_line);
    let prefix_pad = (size - prefix_size) % size;
    let final_size = if first_np_line == 0 {
        temp_size
    } else {
        // We bring down final_size to only the text (no prefix)
        temp_size - (prefix_size + size * (first_np_line - 1))
    };
    let chars: Vec<_> = (0..=255).collect();
    // instead of calling cipher_box every time with the same arguments,
    // we store here the results of the empty_bytes calls, and navigate them.
    let standard_ciphers: Vec<_> = (0..size)
        .map(|i| {
            let mut ith_vec = vec![0; prefix_pad + i];
            cipher_box(&mut ith_vec);
            ith_vec
        })
        .collect();
    let mut res = Vec::with_capacity(final_size);
    let mut char_buff = Vec::with_capacity(4);
    // s is the number of 0-bytes to be added at every step.
    // we start at size-1, then down to 0, and start again.
    let mut s = (0..size).rev().cycle();
    while res.len() != final_size {
        let n = res.len();
        let index = first_np_line + n / size;
        let current_line =
            &standard_ciphers[s.next().unwrap()][(index * size)..((index + 1) * size)];
        let mut test_lines = vec![0; prefix_pad];
        let last_res_line = if n < size - 1 {
            &res[..]
        } else {
            &res[n - (size - 1)..]
        };
        for &c in &chars {
            let mut vec = last_res_line.to_owned();
            vec.push(c);
            utils::pad_bytes(&mut vec, size, false);
            test_lines.append(&mut vec);
        }
        cipher_box(&mut test_lines);

        let idx = test_lines[(first_np_line * size)..(first_np_line + chars.len() * size)]
            .chunks(size)
            .position(|line| line == current_line)
            .unwrap();
        let c = idx as u8;
        // this will print char after char, when we discover them
        // it can be quite lengthy so it's locked behind an environment variable
        if std::env::var("RUSTOPAL_PRINT") == Ok("true".to_string()) {
            utils::may_print_char(c, &mut char_buff);
        }
        res.push(c);
    }
    res
}

/// Given an email, encrypt the corresponding profile using AES-ECB with
/// a constant key.
pub fn crypt_profile(email: String) -> Vec<u8> {
    let mut v = cv::text_to_bytes(utils::cookie_format(utils::create_profile(email)).unwrap());
    aes::encrypt_ecb(RANDOM_KEY.to_vec(), &mut v);
    v
}

/// Parse an encrypted profile into a user map, using the same constant key.
pub fn decrypt_profile(mut profile: Vec<u8>) -> HashMap<String, String> {
    aes::decrypt_ecb(RANDOM_KEY.to_vec(), &mut profile);
    utils::cookie_parse(&cv::bytes_to_text(&profile))
}

/// Create an admin profile using both above functions.
/// A normal crypted profile has this structure:
/// 'email=_&uid=xxx&role=user' where 'email=' is 6 bytes long,
/// and '&uid=xxx&role=user' is 18 bytes long.
/// The encrypted string we want to create looks like this:
/// email=__________
/// __&uid=xxx&role=
/// admin__padding__
/// Since ECB will encrypt a line the same way wherever it is in the text,
/// we first send 10 filler bytes followed by 'admin' and 11 padding bytes,
/// creating this encrypted string:
/// email=fillertext
/// admin__padding__                 <-
/// &uid=054&role=us                  |
/// er______________                  |
/// We then take only the second line |, which we'll put at the end of our made-up
/// string after.
/// To make that made-up string, we just need to take the first 2 lines of the
/// result of encrypting a 12 bytes long email : |
/// email=0123456789                            <-
/// ab&uid=xxx&role=                            <-
/// user____________
pub fn break_profile() -> HashMap<String, String> {
    let filler_string =
        String::from("fillertextadmin\u{b}\u{b}\u{b}\u{b}\u{b}\u{b}\u{b}\u{b}\u{b}\u{b}\u{b}");
    let admin_line = crypt_profile(filler_string)
        .into_iter()
        .skip(16) // Remove the first line
        .take(16); // Take only the second line
    let mut first_lines = crypt_profile(String::from("Amber@is.gr8"));
    first_lines.truncate(32); // Take the first 2 lines
    decrypt_profile(first_lines.into_iter().chain(admin_line).collect())
}

/// Take an arbitrary input string, add something before and after it,
/// and encrypt the result with AES-CBC with the classic random key.
pub fn crypt_comments(text: &str) -> Vec<u8> {
    let before = "comment1=cooking%20MCs;userdata=";
    let escaped = text.replace(";", "_").replace("=", "_");
    let after = ";comment2=%20like%20a%20pound%20of%20bacon";
    let full_text = format!("{}{}{}", before, escaped, after);
    let mut res = cv::text_to_bytes(full_text);
    aes::encrypt_cbc(RANDOM_KEY.to_vec(), &mut res, &[0u8; 16]);
    res
}

/// Take something encrypted with AES-CBC, decrypt it, and look for the string
/// ';admin=true;' in it. Return true if found, false otherwise.
pub fn decrypt_comments(mut crypted: Vec<u8>) -> String {
    aes::decrypt_cbc(RANDOM_KEY.to_vec(), &mut crypted, &[0u8; 16], true);
    let s = cv::bytes_to_text(&crypted);
    format!("Admin: {}", s.contains(";admin=true;"))
}

/// Break the previous functions to make the second one return true.
/// Send 11 0-bytes to the first fun, then flip the bits of the result:
/// By XOR-ing '%20MCs;user' with ';admin=true', we produce some gibberish in the
/// whole block (so 'data=' will also be messed up). But the interesting part is
/// that the block after that, containing our 0-bytes, will also be XOR-ed with
/// ';admin=true'. Since they are 0s, it will produce that very string directly.
pub fn break_comments() -> Vec<u8> {
    let mut encrypted = crypt_comments("\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}");
    for (i, c) in ";admin=true".bytes().enumerate() {
        encrypted[i + 16] ^= c;
    }
    encrypted
}

/// Fetch a random line from the original text,
/// encrypt it under the classic random key,
/// and return the ciphertext and the iv in this order.
pub fn crypt_line() -> (Vec<u8>, Vec<u8>) {
    let mut gen = thread_rng();
    let file = utils::read_file("../cryptopals/resources/data3-1.txt", false);
    let lines: Vec<_> = file.split_ascii_whitespace().collect();
    let line_nb = rand::random::<usize>() % lines.len();
    let chosen_line = lines[line_nb].to_string();
    let mut block = cv::base64_to_bytes(chosen_line);
    let iv = utils::random_bytes(16, Some(&mut gen));
    aes::encrypt_cbc(RANDOM_KEY.to_vec(), &mut block, &iv);
    (block, iv)
}

pub fn check_padding(mut cipher: Vec<u8>, iv: Vec<u8>) -> bool {
    aes::decrypt_cbc(RANDOM_KEY.to_vec(), &mut cipher, &iv, false);
    utils::validate_pkcs7(&cipher).is_ok()
}
