use crate::conversions as cv;
use crate::utils;

/// XOR all the bytes in byte seq 's' against single-byte 'key'.
pub fn single_xor(s: &[u8], key: u8) -> Vec<u8> {
    s.iter().map(|x| x ^ key).collect()
}

/// "Break byte sequence 'input', XORing it against every possible byte.
/// The input is assumed to have been single-key XORed.
pub fn break_single_xor(input: &[u8]) -> (Vec<u8>, u8) {
    let mut min_score = usize::MAX;
    let mut good_key = 0;
    let mut res = input.to_vec();
    for key in 0..=255 {
        let decoded = single_xor(input, key);
        if let Ok(msg) = String::from_utf8(decoded.clone()) {
            let score = utils::weight_freqs(msg);
            if score < min_score {
                min_score = score;
                good_key = key;
                res = decoded;
            }
        }
    }
    (res, good_key)
}

/// Detect which line in given 'input' has been encrypted by single-key xor.
pub fn detect_single_xor(input: String) -> Vec<u8> {
    let iter = input
        .split_whitespace()
        .map(|line| cv::hex_to_bytes(line.to_string()));
    let mut min_score = usize::MAX;
    let mut msg = Vec::new();
    for s in iter {
        let (decoded_line, _) = break_single_xor(&s);
        if let Ok(s) = String::from_utf8(decoded_line.clone()) {
            let score = utils::weight_freqs(s);
            if score < min_score {
                min_score = score;
                msg = decoded_line;
                if score == 0 {
                    break;
                }
            }
        }
    }
    msg
}

/// Sequentially apply (xor) each byte of the key against a byte of the text.
pub fn repeating_xor(key: &[u8], input: &[u8]) -> Vec<u8> {
    input
        .iter()
        .zip(key.iter().cycle())
        .map(|(i, k)| i ^ k)
        .collect()
}

/// Find a likely key size to decrypt a repeating_xor cipher.
/// Helper function for break_repeating_xor.
fn find_key_size(input: &[u8]) -> usize {
    let mut min_score = std::f64::MAX;
    let mut good_size = 0;
    for size in 2..=32 {
        let pairs = input.chunks_exact(2 * size);
        let p_len = pairs.len();
        let score = pairs.fold(0, |sum, pair| {
            sum + utils::hamming_dist(&pair[..size], &pair[size..])
        });
        let pondered_score = (score as f64) / (size * p_len) as f64;
        if pondered_score < min_score {
            min_score = pondered_score;
            good_size = size;
        }
    }
    good_size
}

/// Break repeating-XORed 'input'.
pub fn break_repeating_xor(input: &[u8]) -> (Vec<u8>, Vec<u8>) {
    let size = find_key_size(input);
    let key: Vec<_> = utils::transpose(size, input)
        .iter()
        .map(|line| break_single_xor(line).1)
        .collect();
    let res = repeating_xor(&key, input);
    (res, key)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::conversions as cv;

    #[test]
    fn single_xor_test() {
        let digits = [
            3, 14, 15, 92, 65, 35, 89, 79, 32, 38, 46, 26, 43, 38, 32, 79, 50, 28, 84, 19, 71,
        ];
        assert_eq!(single_xor(&digits, 0), digits);

        let should_be: Vec<u8> = digits
            .iter()
            .map(|&x| if x & 1 == 0 { x + 1 } else { x - 1 })
            .collect();

        assert_eq!(single_xor(&digits, 1), should_be);
    }

    #[test]
    fn break_single_xor_test() {
        let input = [
            27, 55, 55, 51, 49, 54, 63, 120, 21, 27, 127, 43, 120, 52, 49, 51, 61, 120, 57, 120,
            40, 55, 45, 54, 60, 120, 55, 62, 120, 58, 57, 59, 55, 54,
        ];
        let (text, _) = break_single_xor(&input);
        let s = cv::bytes_to_text(&text);
        assert_eq!(s, "Cooking MC's like a pound of bacon");
    }

    #[test]
    fn repeating_xor_test() {
        let v = repeating_xor(
            &[73, 67, 69],
            &cv::text_to_bytes(String::from(
                "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal",
            )),
        );
        let should_be = cv::hex_to_bytes(String::from(
            "0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f"));
        assert_eq!(v, should_be);
    }
}
